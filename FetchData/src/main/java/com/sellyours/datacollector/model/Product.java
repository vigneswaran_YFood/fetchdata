package com.sellyours.datacollector.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

@Id
	private String sku;

	private String name;

	private String model;

	private String SHORT_DESCRIPTION;

	private String DESCRIPTION;

	private int CATEGORY_ID;

	private int TYPE_ID;

	private int SUB_TYPE_ID;

	private int PRICE;

	private int AVAILABLE_QUANTITY;

	private String MANUFACTURER;

	private String THUMBNAILPATH;

	private String IMAGEPATH;

	private int FORSALES;

	private int WEIGHT;

	private String DIMENSION;

	private String WARRANTY;

	private int DISCOUNT;

	private String CREATED_BY;

	private Date CREATED_ON;

	private String UPDATED_BY;

	private Date UPDATED_ON;

	private int COLOR_ID;

	private String BRAND;

	@Override
	public String toString() {
		return "Product{" +
				"sku='" + sku + '\'' +
				", name='" + name + '\'' +
				", model='" + model + '\'' +
				", SHORT_DESCRIPTION='" + SHORT_DESCRIPTION + '\'' +
				", DESCRIPTION='" + DESCRIPTION + '\'' +
				", CATEGORY_ID=" + CATEGORY_ID +
				", TYPE_ID=" + TYPE_ID +
				", SUB_TYPE_ID=" + SUB_TYPE_ID +
				", PRICE=" + PRICE +
				", AVAILABLE_QUANTITY=" + AVAILABLE_QUANTITY +
				", MANUFACTURER='" + MANUFACTURER + '\'' +
				", THUMBNAILPATH='" + THUMBNAILPATH + '\'' +
				", IMAGEPATH='" + IMAGEPATH + '\'' +
				", FORSALES=" + FORSALES +
				", WEIGHT=" + WEIGHT +
				", DIMENSION='" + DIMENSION + '\'' +
				", WARRANTY='" + WARRANTY + '\'' +
				", DISCOUNT=" + DISCOUNT +
				", CREATED_BY='" + CREATED_BY + '\'' +
				", CREATED_ON=" + CREATED_ON +
				", UPDATED_BY='" + UPDATED_BY + '\'' +
				", UPDATED_ON=" + UPDATED_ON +
				", COLOR_ID=" + COLOR_ID +
				", BRAND='" + BRAND + '\'' +
				", SUPPLIER_ID=" + SUPPLIER_ID +
				'}';
	}

	private int SUPPLIER_ID;

	public Product() {
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getSHORT_DESCRIPTION() {
		return SHORT_DESCRIPTION;
	}

	public void setSHORT_DESCRIPTION(String SHORT_DESCRIPTION) {
		this.SHORT_DESCRIPTION = SHORT_DESCRIPTION;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String DESCRIPTION) {
		this.DESCRIPTION = DESCRIPTION;
	}

	public int getCATEGORY_ID() {
		return CATEGORY_ID;
	}

	public void setCATEGORY_ID(int CATEGORY_ID) {
		this.CATEGORY_ID = CATEGORY_ID;
	}

	public int getTYPE_ID() {
		return TYPE_ID;
	}

	public void setTYPE_ID(int TYPE_ID) {
		this.TYPE_ID = TYPE_ID;
	}

	public int getSUB_TYPE_ID() {
		return SUB_TYPE_ID;
	}

	public void setSUB_TYPE_ID(int SUB_TYPE_ID) {
		this.SUB_TYPE_ID = SUB_TYPE_ID;
	}

	public int getPRICE() {
		return PRICE;
	}

	public void setPRICE(int PRICE) {
		this.PRICE = PRICE;
	}

	public int getAVAILABLE_QUANTITY() {
		return AVAILABLE_QUANTITY;
	}

	public void setAVAILABLE_QUANTITY(int AVAILABLE_QUANTITY) {
		this.AVAILABLE_QUANTITY = AVAILABLE_QUANTITY;
	}

	public String getMANUFACTURER() {
		return MANUFACTURER;
	}

	public void setMANUFACTURER(String MANUFACTURER) {
		this.MANUFACTURER = MANUFACTURER;
	}

	public String getTHUMBNAILPATH() {
		return THUMBNAILPATH;
	}

	public void setTHUMBNAILPATH(String THUMBNAILPATH) {
		this.THUMBNAILPATH = THUMBNAILPATH;
	}

	public String getIMAGEPATH() {
		return IMAGEPATH;
	}

	public void setIMAGEPATH(String IMAGEPATH) {
		this.IMAGEPATH = IMAGEPATH;
	}

	public int getFORSALES() {
		return FORSALES;
	}

	public void setFORSALES(int FORSALES) {
		this.FORSALES = FORSALES;
	}

	public int getWEIGHT() {
		return WEIGHT;
	}

	public void setWEIGHT(int WEIGHT) {
		this.WEIGHT = WEIGHT;
	}

	public String getDIMENSION() {
		return DIMENSION;
	}

	public void setDIMENSION(String DIMENSION) {
		this.DIMENSION = DIMENSION;
	}

	public String getWARRANTY() {
		return WARRANTY;
	}

	public void setWARRANTY(String WARRANTY) {
		this.WARRANTY = WARRANTY;
	}

	public int getDISCOUNT() {
		return DISCOUNT;
	}

	public void setDISCOUNT(int DISCOUNT) {
		this.DISCOUNT = DISCOUNT;
	}

	public String getCREATED_BY() {
		return CREATED_BY;
	}

	public void setCREATED_BY(String CREATED_BY) {
		this.CREATED_BY = CREATED_BY;
	}

	public Date getCREATED_ON() {
		return CREATED_ON;
	}

	public void setCREATED_ON(Date CREATED_ON) {
		this.CREATED_ON = CREATED_ON;
	}

	public String getUPDATED_BY() {
		return UPDATED_BY;
	}

	public void setUPDATED_BY(String UPDATED_BY) {
		this.UPDATED_BY = UPDATED_BY;
	}

	public Date getUPDATED_ON() {
		return UPDATED_ON;
	}

	public void setUPDATED_ON(Date UPDATED_ON) {
		this.UPDATED_ON = UPDATED_ON;
	}

	public int getCOLOR_ID() {
		return COLOR_ID;
	}

	public void setCOLOR_ID(int COLOR_ID) {
		this.COLOR_ID = COLOR_ID;
	}

	public String getBRAND() {
		return BRAND;
	}

	public void setBRAND(String BRAND) {
		this.BRAND = BRAND;
	}

	public int getSUPPLIER_ID() {
		return SUPPLIER_ID;
	}

	public void setSUPPLIER_ID(int SUPPLIER_ID) {
		this.SUPPLIER_ID = SUPPLIER_ID;
	}


	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
}